XSLT usage:  
![picture](screen.bmp)  
  
Read from xml with JAXP STaX and converted to json:  
   
[ {
  "id" : 0,
  "name" : "Банк інвестицій та заощаджень",
  "country" : "Україна",
  "activeDeposits" : [ {
    "type" : "терміновий",
    "depositorName" : "Барс-кіт",
    "accountId" : "4123523",
    "amountOnDeposit" : 2.14748365E9,
    "profability" : 1.0,
    "timeConstraints" : 1
  }, {
    "type" : "терміновий",
    "depositorName" : "Окйуліцоп Тсеро",
    "accountId" : "0432423",
    "amountOnDeposit" : 200.0,
    "profability" : 15.0,
    "timeConstraints" : 2415
  }, {
    "type" : "терміновий",
    "depositorName" : "Вася Пупкін",
    "accountId" : "43214",
    "amountOnDeposit" : 0.0,
    "profability" : 3.0,
    "timeConstraints" : 2
  } ]
}, {
  "id" : 1,
  "name" : "Приватбанк",
  "country" : "Україна",
  "activeDeposits" : [ {
    "type" : "терміновий",
    "depositorName" : "Барс-кіт",
    "accountId" : "4123523",
    "amountOnDeposit" : 2.14748365E9,
    "profability" : 1.0,
    "timeConstraints" : 1
  }, {
    "type" : "терміновий",
    "depositorName" : "Окйуліцоп Тсеро",
    "accountId" : "0432423",
    "amountOnDeposit" : 200.0,
    "profability" : 15.0,
    "timeConstraints" : 2415
  }, {
    "type" : "терміновий",
    "depositorName" : "Вася Пупкін",
    "accountId" : "43214",
    "amountOnDeposit" : 0.0,
    "profability" : 3.0,
    "timeConstraints" : 2
  } ]
}, {
  "id" : 2,
  "name" : "Ощадний банк",
  "country" : "Південна Корея",
  "activeDeposits" : [ {
    "type" : "розрахунковий",
    "depositorName" : "Чивеліпоп",
    "accountId" : "414вп",
    "amountOnDeposit" : 33.0,
    "profability" : 1.0,
    "timeConstraints" : 4214
  } ]
} ]