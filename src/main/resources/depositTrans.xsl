<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tfmt {
                    border: 1px ;
                    }

                    td.colfmt {
                    border: 1px ;
                    background-color: light-grey;
                    text-align:center;
                    }

                    th {
                    background-color: #2E9AFE;
                    color: white;
                    }

                    .center {
                    width: 44%;
                    border: 1px solid green;
                    padding: 10px;
                    text-align:center;
                    }


                </style>
            </head>
            <body>
                <xsl:for-each select="/banks/bank">
                    <div>
                        <div class="center">
                            <xsl:value-of select="name"/>
                        </div>
                        <div class="center">
                            <xsl:value-of select="country"/>
                        </div>
                    </div>
                    <table class="tfmt">
                        <tr>
                            <th>Type</th>
                            <th>Depositor name</th>
                            <th>Account ID</th>
                            <th>Amount on deposit</th>
                            <th>Profability</th>
                            <th>Time constraints</th>
                        </tr>
                        <xsl:for-each select="activeDeposits/deposit">
                            <tr>
                                <td class="colfmt">
                                    <xsl:value-of select="type"/>
                                </td>
                                <td class="colfmt">
                                    <xsl:value-of select="depositorName"/>
                                </td>
                                <td class="colfmt">
                                    <xsl:value-of select="accountId"/>
                                </td>
                                <td class="colfmt">
                                    <xsl:value-of select="amountOnDeposit"/>
                                </td>
                                <td class="colfmt">
                                    <xsl:value-of select="profability"/>
                                </td>
                                <td class="colfmt">
                                    <xsl:value-of select="timeConstraints"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </table>
                    <div style="margin-bottom:20px"></div>
                </xsl:for-each>

                <!--<table class="tfmt">-->
                <!--<tr>-->
                <!--<th style="width:250px">Name</th>-->
                <!--<th style="width:350px">Country</th>-->

                <!--</tr>-->
                <!--</table>-->

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>