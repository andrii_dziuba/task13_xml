package parser;


import com.fasterxml.jackson.databind.ObjectMapper;
import comparator.BankComparator;
import filechecker.ExtensionChecker;
import model.Bank;
import parser.stax.StAXReader;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Parser {

    public static void main(String[] args) {
        File xml = new File("src\\main\\resources\\deposits.xml");
        File xsd = new File("src\\main\\resources\\deposits.xsd");

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            List<Bank> bankList = StAXReader.parseBanks(xml, xsd);

            ObjectMapper objectMapper = new ObjectMapper();
            try {
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(System.out, bankList);
            } catch (IOException e) {
                e.printStackTrace();
            }

//            printList(bankList, "StAX");
        }
    }

    private static boolean checkIfXSD(File xsd) {
        return ExtensionChecker.isXSD(xsd);
    }

    private static boolean checkIfXML(File xml) {
        return ExtensionChecker.isXML(xml);
    }

    private static void printList(List<Bank> banks, String parserName) {
        Collections.sort(banks, new BankComparator());
        System.out.println(parserName);
        for (Bank bank : banks) {
            System.out.println(bank);
        }
    }

}
