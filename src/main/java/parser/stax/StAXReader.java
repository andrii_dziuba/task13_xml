package parser.stax;

import model.Bank;
import model.Deposit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StAXReader {
    public static List<Bank> parseBanks(File xml, File xsd){
        List<Bank> bankList = new ArrayList<>();
        Bank bank = null;
        Deposit deposit = null;
        List<Deposit> deposits = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "bank":
                            bank = new Bank();

                            Attribute idAttr = startElement.getAttributeByName(new QName("id"));
                            if (idAttr != null) {
                                bank.setId(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "country":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert bank != null;
                            bank.setCountry(xmlEvent.asCharacters().getData());
                            break;
                        case "activeDeposits":
                            xmlEvent = xmlEventReader.nextEvent();
                            deposits = new ArrayList<>();
                            bank.setActiveDeposits(deposits);
                            break;
                        case "deposit":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert deposits != null;
                            deposit = new Deposit();
                            deposits.add(deposit);
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            deposit.setType(xmlEvent.asCharacters().getData());
                            break;
                        case "depositorName":
                            xmlEvent = xmlEventReader.nextEvent();
                            deposit.setDepositorName(xmlEvent.asCharacters().getData());
                            break;
                        case "accountId":
                            xmlEvent = xmlEventReader.nextEvent();
                            deposit.setAccountId(xmlEvent.asCharacters().getData());
                            break;
                        case "amountOnDeposit":
                            xmlEvent = xmlEventReader.nextEvent();
                            deposit.setAmountOnDeposit(Float.parseFloat(xmlEvent.asCharacters().getData()));
                            break;
                        case "profability":
                            xmlEvent = xmlEventReader.nextEvent();
                            deposit.setProfability(Float.parseFloat(xmlEvent.asCharacters().getData()));
                            break;
                        case "timeConstraints":
                            xmlEvent = xmlEventReader.nextEvent();
                            deposit.setTimeConstraints(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("bank")){
                        bankList.add(bank);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return bankList;
    }
}
