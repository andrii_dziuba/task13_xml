package comparator;

import model.Bank;

import java.util.Comparator;

public class BankComparator implements Comparator<Bank> {
    public int compare(Bank o1, Bank o2) {
        return o1.getName().compareToIgnoreCase(o2.getName());
    }
}
