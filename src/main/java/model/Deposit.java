package model;


public class Deposit {
    private String type;
    private String depositorName;
    private String accountId;
    private float amountOnDeposit;
    private float profability;
    private int timeConstraints;

    public Deposit() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public float getAmountOnDeposit() {
        return amountOnDeposit;
    }

    public void setAmountOnDeposit(float amountOnDeposit) {
        this.amountOnDeposit = amountOnDeposit;
    }

    public float getProfability() {
        return profability;
    }

    public void setProfability(float profability) {
        this.profability = profability;
    }

    public int getTimeConstraints() {
        return timeConstraints;
    }

    public void setTimeConstraints(int timeConstraints) {
        this.timeConstraints = timeConstraints;
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "type='" + type + '\'' +
                ", depositorName='" + depositorName + '\'' +
                ", accountId='" + accountId + '\'' +
                ", amountOnDeposit=" + amountOnDeposit +
                ", profability=" + profability +
                ", timeConstraints=" + timeConstraints +
                '}';
    }
}
