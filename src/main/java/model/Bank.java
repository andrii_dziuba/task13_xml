package model;

import java.util.List;

public class Bank {

    private int id;
    private String name;
    private String country;

    private List<Deposit> activeDeposits;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Deposit> getActiveDeposits() {
        return activeDeposits;
    }

    public void setActiveDeposits(List<Deposit> activeDeposits) {
        this.activeDeposits = activeDeposits;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", activeDeposits=" + activeDeposits +
                '}';
    }
}
